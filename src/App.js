import React, {useState, useEffect} from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

// Context
import UserContext from './UserContext';

/*components*/
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
// import CourseCard from './components/CourseCard';
// import Welcome from './components/Welcome'
import Courses from './pages/Courses';
// import Counter from './components/Counter'
import Register from './pages/Register';
import LogIn from './pages/LogIn';
import Error from './components/Error'
import SpecificCourse from './pages/SpecificCourse'
import AddCourse from './pages/AddCourse';

export default function App(){

	const [user, setUser] = useState({
		id: null,
		isAdmin: null
	});

	const unsetUser =() => {
		localStorage.clear();
		setUser({
			id: null,
			isAdmin: null
		})
	}

	useEffect(()=>{
		let token = localStorage.getItem('token')
		fetch('https://radiant-taiga-63523.herokuapp.com/api/users/details',{
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			console.log(result)//object or document of user
			if(typeof result._id !== "undefined"){
				setUser({
					id: result._id,
					isAdmin: result.isAdmin
				})
			} else {
				setUser({
					id: null,
					isAdmin: null
				})
			}
		})
	}, [])

	return(
		// <Fragment>
		//   <AppNavbar/>
		//   <Home/>
		//   <CourseCard/>
		//  <Welcome name="John"/>
		//   <Welcome name="Lawrence"/>
		//   <Courses/>
		//   <Counter/>
		//  <Register/>
		//  <LogIn/>
		// </Fragment>
	<UserContext.Provider value={{user, setUser, unsetUser}}>
		<BrowserRouter>
			<AppNavbar/>				{/*//first user is props from APpnavbar}*/}
			<Switch>
				<Route exact path ="/" component={Home} />
				<Route exact path ="/courses" component={Courses} />
				<Route exact path ="/register" component={Register} />
				<Route exact path ="/login" component={LogIn} />
				<Route exact path ="/courses/:courseId" component={SpecificCourse}/>
				<Route exact path="/addCourse" component={AddCourse} />
				<Route component={Error}/>
			</Switch>
		</BrowserRouter>
	</UserContext.Provider>
	)
}