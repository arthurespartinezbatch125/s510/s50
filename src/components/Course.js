import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';

import {Link} from 'react-router-dom'

/*react-bootstrap components*/
import {Card, Button} from 'react-bootstrap';
export default function Course({courseProp}){
	// console.log(props)
	console.log(courseProp)

	const {name, description, price, _id} = courseProp

	// OR courseProp.name = name
	// console.log(name)

	// let course = props.course

	//[state, setState] = useState()
	// const [count, setCount] = useState(0);
	// const [seat, setSeat] = useState(10);
	// const [isDisabled, setIsDisabled] = useState(false)

	// function enroll(){				//handler
	// 	if(seat>0){
	// 		setCount(count+1)
	// 		setSeat(seat-1)
	// 	} else {
	// 		setIsDisabled(true)
	// 	}
	// }

	// useEffect(()=>{
	// 	if(seat === 0){
	// 		setIsDisabled(true)
	// 	}
	// },[seat]);

	return(
		<Card className="mb-3">
		  <Card.Body>
		  	<Card.Title>{name}</Card.Title>
		  	<h5>Description</h5>
		  	<p>{description}</p>
		  	<h5>Price:</h5>
		  	<p>{price}</p>
		  	{/*<h5>Enrollees</h5>
		  	<p>{count} Enrollees</p>
		  	<h5>Seats</h5>
		  	<p>{seat}</p>
		    <Button variant="primary" onClick={enroll
		    	//ternary operator
		    	// ()=>{count < 30 ? setCount(count+1) : alert("No slots available")} 
		    } disabled={isDisabled} >Enroll</Button>*/}
		    <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
		  </Card.Body>
		</Card>
	)
}

Course.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}