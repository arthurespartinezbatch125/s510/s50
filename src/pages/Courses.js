import React, {useState, useEffect, useContext} from 'react';
import {Container} from 'react-bootstrap';


// import courses from './../mock-data/courses';

import AdminView from './../components/AdminView';
import UserView from './../components/UserView'

import UserContext from './../UserContext';


export default function Courses(){

	const [courses, setCourses] = useState([]);

	const {user} = useContext(UserContext);

	const fetchData = () => {

		let token = localStorage.getItem('token')
		fetch('https://radiant-taiga-63523.herokuapp.com/api/courses/all', {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(result => {
			// console.log(result)
			setCourses(result)
		})
	}
	useEffect(()=>{
		fetchData()
	}, [])
	//display courses using map
	// let CourseCards = courses.map((course) => {
	// 	return <Course key={course.id} course={course}/>
	// })
	return (
		<Container className="p-4">
			{ (user.isAdmin === true) ? <AdminView courseData = {courses} fetchData={fetchData}/> :
			 <UserView courseData={courses}/> }
		</Container>
	)
}