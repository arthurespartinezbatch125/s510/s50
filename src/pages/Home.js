import React from 'react'

import Container from 'react-bootstrap/Container'

// Components
import Banner from './../components/Banner';
import Highlights from './../components/Highlights';

export default function Home(){
	return(
		<Container fluid>
			<Banner/>
			<Highlights/>
		</Container>
	)
}